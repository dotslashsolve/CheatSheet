# History

### shorcut command : `!previous_command`

```bash
> ssh test@123.345.789 ...
> ...
> !ssh
```
this will run last ssh command

### get last argument : `!!:argument_index`
    
```bash
> ls ~/Github/AutoScripts
> cd !!:1
```
this will get autocompleted to `cd ~/Github/AutoScripts`

*Note:*
`!!:$` > shortcut for > `!!:last_index`
