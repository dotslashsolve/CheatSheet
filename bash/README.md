# Index

### [History](History.md)

### keyboard Shortcuts

#### `Ctrl+a` : Move to begining of line
#### `Ctrl+u` : delete current line
#### `ctrl+w` : delete word

deletes the closest word before the cursor ie:

`curl -X GET http://localhost:8080'ctrl+w'`
 
 will produce

`curl -X GET http://localhost:` 
 